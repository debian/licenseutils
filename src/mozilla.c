/*  Copyright (C) 2013, 2014 Ben Asselstine

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
  02110-1301, USA.
*/
#include <config.h>
#include <stdlib.h>
#include <unistd.h>
#include "licensing_priv.h"
#include "mozilla.h"
#include "gettext-more.h"
#include "xvasprintf.h"
#include "read-file.h"
#include "error.h"
#include "util.h"
#include "url-downloader.h"

static struct argp_option argp_options[] = 
{
    {"v1.1", '1', NULL, 0, N_("show version 1.1 of the MPL")},
    {"v2.0", '2', NULL, 0, N_("show version 2.0 of the MPL")},
    {"full", 'f', NULL, 0, N_("show the full license text")},
    {"list-license-notices", 'l', NULL, OPTION_HIDDEN, N_("show licenses and exit")},
    {0}
};

static error_t 
parse_opt (int key, char *arg, struct argp_state *state)
{
  struct lu_mpl_options_t *opt = NULL;
  if (state)
    opt = (struct lu_mpl_options_t*) state->input;
  switch (key)
    {
    case 'f':
      opt->full = 1;
      break;
    case '1':
      opt->version = 1; //1.1 actually
      break;
    case '2':
      opt->version = 2;
      break;
    case 'l':
        {
          int i = 0;
          while (mpl.notices[i].keyword != NULL)
            {
              fprintf (stdout, "%s\n", mpl.notices[i].keyword);
              i++;
            }
          exit (0);
        }
      break;
    case ARGP_KEY_INIT:
      opt->version = 2;
      break;
    default:
      return ARGP_ERR_UNKNOWN;
    }
  return 0;
}

#undef MPL_DOC
#define MPL_DOC N_("Show the Mozilla Public License notice.")
static struct argp argp = { argp_options, parse_opt, "", MPL_DOC};

int 
lu_mpl_parse_argp (struct lu_state_t *state, int argc, char **argv)
{
  int err = 0;
  struct lu_mpl_options_t opts;
  opts.state = state;

  err = argp_parse (&argp, argc, argv, state->argp_flags,  0, &opts);
  if (!err)
    return lu_mpl (state, &opts);
  else
    return err;
}

int
show_lu_mpl (struct lu_state_t *state, struct lu_mpl_options_t *options)
{
  char *url;
  if (options->version == 2)
    url = strdup ("https://www.mozilla.org/media/MPL/2.0/index.txt");
  else if (options->version == 1)
    url = strdup ("https://www.mozilla.org/media/MPL/1.1/index.txt");
  else 
    url = strdup ("https://www.mozilla.org/media/MPL/2.0/index.txt");
  char *data = NULL;
  int err = download (state, url, &data);
  free (url);



  if (options->full)
    {
      luprintf (state, "%s\n", data);
      return err;
    }

  replace_html_entities (data);
  if (options->version == 2)
    err = show_lines_after (state, data,
                            "This Source Code Form is subject to the terms of the Mozilla", 
                            3, 1, "  ", "");
  else if (options->version == 1)
    err = show_lines_after (state, data,
                            "The contents of this file are subject to the Mozilla",
                            9, 1, "     ", "");
  else
    err = show_lines_after (state, data,
                            "This Source Code Form is subject to the terms of the Mozilla",
                            3, 1, "  ", "");
  free (data);
  return err;
}

int 
lu_mpl (struct lu_state_t *state, struct lu_mpl_options_t *options)
{
  int err = 0;
  err = show_lu_mpl (state, options);
  return err;
}

struct lu_command_t mpl = 
{
  .name         = N_("mpl"),
  .doc          = MPL_DOC,
  .flags        = IS_A_LICENSE | SAVE_IN_HISTORY | SHOW_IN_HELP,
  .argp         = &argp,
  .parser       = lu_mpl_parse_argp,
  .latest_idx   = 2,
  .mentions     =
    {
      " the MPL ",
      " Mozilla public license ",
      " Mozilla license ",
      NULL
    },
  .notices      =
    {
        {
          .keyword   = "mplv2.0,full-license",
          .cmd       = "mpl -2 --full",
          .words     = "Mozilla,Version 2.0",
          .recommend = 1
        },
        {
          .keyword   = "mplv1.1,full-license",
          .cmd       = "mpl -1 --full",
          .words     = "MOZILLA,Version 1.1",
          .recommend = 0
        },
        {
          .keyword   = "mplv2.0",
          .cmd       = "mpl",
          .words     = "Mozilla,2.0",
          .recommend = 1
        },
        {
          .keyword   = "mplv1.1",
          .cmd       = "mpl -1",
          .words     = "Mozilla,1.1",
          .recommend = 0
        },
        {
          .keyword   = NULL,
          .cmd       = NULL,
          .words     = NULL,
          .recommend = 0
        },
    }
};

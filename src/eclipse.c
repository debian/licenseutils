/*  Copyright (C) 2017 Ben Asselstine

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
  02110-1301, USA.
*/
#include <config.h>
#include <stdlib.h>
#include <unistd.h>
#include "licensing_priv.h"
#include "eclipse.h"
#include "gettext-more.h"
#include "xvasprintf.h"
#include "read-file.h"
#include "error.h"
#include "util.h"
#include "url-downloader.h"

static struct argp_option argp_options[] = 
{
    {"v1.0", '1', NULL, 0, N_("show version 1.1 of the EPL")},
    {"v2.0", '2', NULL, 0, N_("show version 2.0 of the EPL")},
    {"full", 'f', NULL, 0, N_("show the full license text")},
    {"list-license-notices", 'l', NULL, OPTION_HIDDEN, N_("show licenses and exit")},
    {0}
};

static error_t 
parse_opt (int key, char *arg, struct argp_state *state)
{
  struct lu_epl_options_t *opt = NULL;
  if (state)
    opt = (struct lu_epl_options_t*) state->input;
  switch (key)
    {
    case 'f':
      opt->full = 1;
      break;
    case '1':
      opt->version = 1;
      break;
    case '2':
      opt->version = 2;
      break;
    case 'l':
        {
          int i = 0;
          while (epl.notices[i].keyword != NULL)
            {
              fprintf (stdout, "%s\n", epl.notices[i].keyword);
              i++;
            }
          exit (0);
        }
      break;
    case ARGP_KEY_INIT:
      opt->version = 2;
      break;
    default:
      return ARGP_ERR_UNKNOWN;
    }
  return 0;
}

#undef EPL_DOC
#define EPL_DOC N_("Show the Eclipse Public License notice.")
static struct argp argp = { argp_options, parse_opt, "", EPL_DOC};

int 
lu_epl_parse_argp (struct lu_state_t *state, int argc, char **argv)
{
  int err = 0;
  struct lu_epl_options_t opts;
  opts.state = state;

  err = argp_parse (&argp, argc, argv, state->argp_flags,  0, &opts);
  if (!err)
    return lu_epl (state, &opts);
  else
    return err;
}

int
show_lu_epl (struct lu_state_t *state, struct lu_epl_options_t *options)
{
  if (options->full)
    {
      char *url;
      if (options->version == 2)
        url =
          strdup ("http://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.txt");
      else if (options->version == 1)
        url =
          strdup ("https://directory.fsf.org/wiki/License:EPLv1.0");
      else 
        url =
          strdup ("http://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.txt");

      char *data = NULL;
      int err = download (state, url, &data);
      free (url);

      if (options->version == 1)
        {
          replace_html_entities (data);
          err = show_lines_after (state, data,
                                  "\nEclipse Public License - v 1.0\n", 
                                  231, 0, NULL, NULL);
        }
      else
        luprintf (state, "%s\n", data);
      free (data);
      return err;
    }

  switch (options->version)
    {
    case 1:
      luprintf (state,"\
All rights reserved. This program and the accompanying materials\n\
are made available under the terms of the Eclipse Public License v1.0\n\
which accompanies this distribution, and is available at\n\
http://www.eclipse.org/legal/epl-v10.html\n");
      break;
    case 2:
    default:
      luprintf (state,"\
All rights reserved. This program and the accompanying materials\n\
are made available under the terms of the Eclipse Public License v2.0\n\
which accompanies this distribution, and is available at\n\
http://www.eclipse.org/legal/epl-v20.html\n");
      break;
    }
  return 0;
}

int 
lu_epl (struct lu_state_t *state, struct lu_epl_options_t *options)
{
  int err = 0;
  err = show_lu_epl (state, options);
  return err;
}

struct lu_command_t epl = 
{
  .name         = N_("epl"),
  .doc          = EPL_DOC,
  .flags        = IS_A_LICENSE | SAVE_IN_HISTORY | SHOW_IN_HELP,
  .argp         = &argp,
  .parser       = lu_epl_parse_argp,
  .latest_idx   = 2,
  .mentions     =
    {
      " the EPL ",
      " Eclipse license ",
      NULL
    },
  .notices      =
    {
        {
          .keyword   = "eplv2.0,full-license",
          .cmd       = "epl -2 --full",
          .words     = "Eclipse,v 2.0,Steward",
          .recommend = 0
        },
        {
          .keyword   = "eplv1.0,full-license",
          .cmd       = "epl -1 --full",
          .words     = "Eclipse,v 1.0,New York",
          .recommend = 0
        },
        {
          .keyword   = "eplv2.0",
          .cmd       = "epl",
          .words     = "Eclipse,v2.0",
          .recommend = 0
        },
        {
          .keyword   = "eplv1.0",
          .cmd       = "epl -1",
          .words     = "Eclipse,v1.0",
          .recommend = 0
        },
        {
          .keyword   = NULL,
          .cmd       = NULL,
          .words     = NULL,
          .recommend = 0
        },
    }
};

/*  Copyright (C) 2017 Ben Asselstine

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
  02110-1301, USA.
*/
#include <config.h>
#include <stdlib.h>
#include <unistd.h>
#include "licensing_priv.h"
#include "zlib.h"
#include "gettext-more.h"
#include "xvasprintf.h"
#include "read-file.h"
#include "error.h"
#include "util.h"
#include "url-downloader.h"

static struct argp_option argp_options[] = 
{
    {"list-license-notices", 'l', NULL, OPTION_HIDDEN, N_("show licenses and exit")},
    {0}
};

static error_t 
parse_opt (int key, char *arg, struct argp_state *state)
{
  switch (key)
    {
    case 'l':
        {
          int i = 0;
          while (zlib.notices[i].keyword != NULL)
            {
              fprintf (stdout, "%s\n", zlib.notices[i].keyword);
              i++;
            }
          exit (0);
        }
      break;
    default:
      return ARGP_ERR_UNKNOWN;
    }
  return 0;
}

#undef ZLIB_DOC
#define ZLIB_DOC N_("Show the ZLib License.")
static struct argp argp = { argp_options, parse_opt, "", ZLIB_DOC};

int 
lu_zlib_parse_argp (struct lu_state_t *state, int argc, char **argv)
{
  int err = 0;
  struct lu_zlib_options_t opts;
  opts.state = state;

  err = argp_parse (&argp, argc, argv, state->argp_flags,  0, &opts);
  if (!err)
    return lu_zlib (state, &opts);
  else
    return err;
}

int
show_lu_zlib(struct lu_state_t *state, struct lu_zlib_options_t *options)
{
  char *url = strdup ("https://directory.fsf.org/wiki/License:Zlib");
  char *data = NULL;
  int err = download (state, url, &data);
  free (url);

  replace_html_entities (data);
  err = show_lines_after (state, data, "  This software is provided ", 15, 0, 
                          NULL, NULL);
  free (data);
  return err;
}

int 
lu_zlib (struct lu_state_t *state, struct lu_zlib_options_t *options)
{
  int err = 0;
  err = show_lu_zlib(state, options);
  return err;
}

struct lu_command_t zlib = 
{
  .name         = N_("zlib"),
  .doc          = ZLIB_DOC,
  .flags        = IS_A_LICENSE | SAVE_IN_HISTORY,
  .argp         = &argp,
  .parser       = lu_zlib_parse_argp,
  .latest_idx   = 0,
  .mentions     =
    {
      " zlib license ",
      NULL
    },
  .notices      =
    {
        {
          .keyword   = "zlib",
          .cmd       = "zlib",
          .words     = "origin,misrepresented",
          .recommend = 1
        },
        {
          .keyword   = NULL,
          .cmd       = NULL,
          .words     = NULL,
          .recommend = 0
        },
    }
};

/*  Copyright (C) 2014, 2017 Ben Asselstine

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
  02110-1301, USA.
*/
#include <config.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <argz.h>
#include <sys/types.h>
#include <dirent.h>

#include "licensing_priv.h"
#include "detect.h"
#include "gettext-more.h"
#include "xvasprintf.h"
#include "boilerplate.h"
#include "png-boilerplate.h"
#include "uncomment.h"
#include "opts.h"
#include "read-file.h"
#include "error.h"
#include "copy-file.h"
#include "fstrcmp.h"
#include "findprog.h"
#include "argz.h"
#include "trim.h"
#include "util.h"
#include "ctype.h"

enum
{
  OPT_FULL = -208,
  OPT_MENTIONS,
};

static struct argp_option argp_options[] = 
{
    { "mentions", OPT_MENTIONS, NULL, 0, N_("also detect references to licenses")},
    { "full", OPT_FULL, NULL, 0, N_("detect in all comments instead of topmost")},
    { "show-all-licenses", 'a', NULL, 0, N_("show a full list of how the licenses match")},
    {"limit", 'l', "PERCENT", 0, N_("show matching licenses over PERCENT (default 75)")},
    {"no-cull", 'n', NULL, OPTION_HIDDEN, N_("do not cull results")},
    { 0 }
};

static error_t 
parse_opt (int key, char *arg, struct argp_state *state)
{
  struct lu_detect_options_t *opt = NULL;
  if (state)
    opt = (struct lu_detect_options_t*) state->input;
  switch (key)
    {
    case 'n':
      opt->cull = 0;
      break;
    case OPT_MENTIONS:
      opt->mentions = 1;
      break;
    case OPT_FULL:
      opt->full = 1;
      break;
    case 'l':
        {
          int percent = atoi (arg);
          if (percent > 100 || percent < 0)
            argp_failure (state, 0, 0, "percent must be between 0 and 100");
          else
            opt->limit = percent;
        }
      break;
    case 'a':
      opt->show_most_similar_licenses = 0;
      break;
    case ARGP_KEY_ARG:
      if (access (arg, R_OK) == 0)
        {
          DIR *d = opendir (arg);
          if (d)
            closedir (d);
          else
            argz_add (&opt->input_files, &opt->input_files_len, arg);
        }
      else if (strcmp (arg, "-") != 0)
        argp_failure (state, 0, 0, "could not open `%s' for reading (%m)", arg);
      break;
    case ARGP_KEY_INIT:
      opt->cull = 1;
      opt->full = 0;
      opt->mentions = 0;
      opt->input_files = NULL;
      opt->input_files_len = 0;
      opt->show_most_similar_licenses = 1;
      opt->limit = 75.0;
      break;
    case ARGP_KEY_FINI:
      if (opt->input_files == NULL)
        argz_add (&opt->input_files, &opt->input_files_len, "-");
      break;
    default:
      return ARGP_ERR_UNKNOWN;
    }
  return 0;
}

#undef DETECT_DOC
#define DETECT_DOC \
  N_("Statistically determine the license notice of a file.") "\v"\
  N_("With no FILE, or when FILE is -, it is read from standard input.") "  " \
  N_("When FILE is given on the command line it is passed through the boilerplate command, and the uncomment command, while the standard input is not.")
static struct argp argp = { argp_options, parse_opt, "[FILE...]", DETECT_DOC};

int 
lu_detect_parse_argp (struct lu_state_t *state, int argc, char **argv)
{
  int err = 0;
  struct lu_detect_options_t opts;
  opts.state = state;
  err = argp_parse (&argp, argc, argv, state->argp_flags,  0, &opts);
  if (!err)
    return lu_detect (state, &opts);
  else
    return err;
}

struct license_result_t
{
  char *license_family;
  int starting_block;
  int ending_block;
  char *license;
  char *cmd;
  float result;
  size_t matchlen;
};

int
compare_license_results (const void *lhs, const void *rhs)
{
  struct license_result_t *r = (struct license_result_t*) rhs;
  struct license_result_t *l = (struct license_result_t*) lhs;
  if (r->result == l->result)
    {
      int lrange = l->ending_block - l->starting_block + 1;
      int rrange = r->ending_block - r->starting_block + 1;
      if (lrange - rrange == 0)
        return r->matchlen - l->matchlen;
      return lrange - rrange;
    }
  else if (l->result < r->result)
    return 1;
  else
    return -1;
}

//remove whitespace from string, lowercasify,
//and retain spaces between keywords
static char *
squeeze (char *s)
{
  int i = 0;
  char *letter;
  size_t len = strlen (s);
  if (len < 2)
    len = 2;
  char *result = malloc (len + 1);
  result[0] = ' ';
  result[1] = '\0';
  int separated = 1;
  for (letter = &s[0]; *letter != '\0'; letter++)
    {
      int keyword_separator = 0;
      if (isspace (*letter) || ispunct (*letter))
        keyword_separator = 1;
      if (keyword_separator && !separated)
        {
          result[i] = ' ';
          separated = 1;
        }
      else if (!keyword_separator)
        {
          result[i] = tolower(*letter);
          separated = 0;
        }
      else
        continue;
      i++;
    }
  result[i] = '\0';
  return result;
}

static float
sherlock (char *squeezed_license, char *data)
{
  double results;
  //size_t slen1 = strlen (squeezed_license);
  //size_t slen2 = strlen (data);
  //don't even bother if the sizes are way off.
  //if (slen2 * 4 < slen1)
    //results = 0;
  if (strstr (data, squeezed_license) == NULL)
    results = fstrcmp (squeezed_license, data);
  else
    results = 1;
  return results;
}

static int
dump_blocks (FILE *in, FILE *out, lu_block_t *blocks, int num_blocks, int start, int end)
{
  int err = 0;
  char *line = NULL;
  size_t len = 0;
  ssize_t read;
  int linecount = 0;

  while ((read = getline (&line, &len, in)) != -1)
    {
      int inside = 0;
      int needs_newline = 0;
      for (int i = start; i <= end; i++)
        {
          if (linecount >= blocks[i].starting_line &&
              linecount <= blocks[i].ending_line)
            {
              inside = 1;
              if (linecount == blocks[i].ending_line && i != end)
                needs_newline = 1;
              break;
            }
        }
      if (inside)
        {
          fprintf (out, "%s", line);
          if (needs_newline)
            fprintf (out, "\n");
        }
      linecount++;
    }
  free (line);
  return err;
}

static int
detect_licenses_and_collect_results (struct lu_state_t *state, struct lu_detect_options_t *options, char *unsqueezed_data, char *data, struct license_result_t **results, int *num_results, int starting_block, int ending_block, char **lic_cache, struct lu_license_t *licenses, int num_licenses, struct lu_license_mention_t *mentions, int num_mentions, char **mention_cache)
{
  int count = 0;
  //collect results
  int i = 0;
  while (licenses[i].keyword != NULL)
    {
      char *argz = NULL;
      size_t argz_len = 0;
      argz_create_sep (licenses[i].words, ',', &argz, &argz_len);
      int found_necessary_keyword = 0;
      char *k = NULL;
      while ((k = argz_next (argz, argz_len, k)))
        {
          if (strstr (unsqueezed_data, k))
            {
              found_necessary_keyword = 1;
              break;
            }
        }
      free (argz);
      if (!found_necessary_keyword)
        {
          count++;
          i++;
          continue;
        }

      char *cmd = licenses[i].cmd;
      float result = sherlock (lic_cache[count], data) * 100;
      *results = realloc (*results, (*num_results + 1) *
                          sizeof (struct license_result_t));
      (*results)[*num_results].license_family =
        strdup (licenses[i].license_family);
      (*results)[*num_results].license = strdup (licenses[i].keyword);
      (*results)[*num_results].cmd = strdup (cmd);
      (*results)[*num_results].result = result;
      (*results)[*num_results].starting_block = starting_block;
      (*results)[*num_results].ending_block = ending_block;
      (*results)[*num_results].matchlen = strlen (lic_cache[count]);
      (*num_results)++;
      count++;
      i++;
    }

  if (options->mentions)
    {
      i = 0;
      while (mentions[i].words)
        {
          if (strstr (data, mention_cache[i]))
            {
              *results = realloc (*results, (*num_results + 1) *
                                  sizeof (struct license_result_t));
              (*results)[*num_results].license_family =
                strdup (mentions[i].license_family);
              (*results)[*num_results].license =
                xasprintf ("%s-mention", mentions[i].license_family);
              (*results)[*num_results].cmd = strdup("no command");
              (*results)[*num_results].result = options->limit;
              (*results)[*num_results].starting_block = starting_block;
              (*results)[*num_results].ending_block = ending_block;
              (*results)[*num_results].matchlen = strlen (mention_cache[i]);
              (*num_results)++;
            }
          i++;
        }
    }

  return 0;
}

static char *
dump_file (char *source_file, lu_block_t *blocks, int num_blocks, int start, int end)
{
  FILE *in = fopen (source_file, "r");
  if (!in)
    return NULL;
  char tmp[sizeof(PACKAGE) + 13];
  snprintf (tmp, sizeof tmp, "/tmp/%s.XXXXXX", PACKAGE);
  int fd = mkstemp(tmp);
  close (fd);
  FILE *out = fopen (tmp, "w");

  int err = 0;
  if (out)
    {
      err = dump_blocks (in, out, blocks, num_blocks, start, end);
      fclose (out);
    }
  fclose (in);
  if (err)
    return NULL;
  return strdup (tmp);
}

static void
cull_results (struct lu_detect_options_t *options, struct license_result_t **results, int *num_results)
{
  struct license_result_t *r = NULL;
  int num_r = 0;
  qsort (*results, *num_results, sizeof (struct license_result_t), compare_license_results);

  if (options->show_most_similar_licenses)
    {
      //phase 1.  contiguous matches have to go.
      for (int i = 0; i < *num_results; i++)
        {
          int found = 0;
          for (int j = 0; j < num_r; j++)
            {
              //do we overlap an ending region?
              if ((*results)[i].starting_block <= r[j].ending_block &&
                  (*results)[i].ending_block >= r[j].ending_block)
                {
                  found = 1;
                  if ((*results)[i].starting_block == r[j].ending_block)
                    found = 0;
                  if ((*results)[i].ending_block == r[j].ending_block)
                    found = 0;
                  //we allow an overlap by 1.
                }
              //do we overlap a starting region?
              if ((*results)[i].starting_block <= r[j].starting_block &&
                  (*results)[i].ending_block >= r[j].starting_block)
                {
                  found = 1;
                  if ((*results)[i].ending_block >= r[j].starting_block)
                    found = 0;
                  if ((*results)[i].ending_block == r[j].starting_block)
                    found = 0;
                  //we allow an overlap by 1.
                }
              if ((*results)[i].starting_block == r[j].starting_block &&
                  (*results)[i].ending_block == r[j].ending_block)
                found = 1;
              if (strstr (r[j].license, "mention"))
                found = 0;
              if (strstr ((*results)[i].license, "mention"))
                found = 0;
              if (found)
                break;
            }
          if (!found)
            {
              r = realloc (r, (num_r + 1) * sizeof (struct license_result_t));
              r[num_r] = (*results)[i];
              r[num_r].license_family = strdup ((*results)[i].license_family);
              r[num_r].license = strdup ((*results)[i].license);
              r[num_r].cmd = strdup ((*results)[i].cmd);
              num_r++;
            }
        }
      for (int i = 0; i < *num_results; i++)
        {
          free ((*results)[i].license_family);
          free ((*results)[i].license);
          free ((*results)[i].cmd);
        }
      free (*results);
      *results = r;
      *num_results = num_r;
    }

  //phase 2.  same licenses matched in other places have to go.
  r = NULL;
  num_r = 0;
  for (int i = 0; i < *num_results; i++)
    {
      int found = 0;
      for (int j = 0; j < num_r; j++)
        {
          if (strcmp (r[j].license, (*results)[i].license) == 0)
            found = 1; 
        }
      if (!found)
        {
          r = realloc (r, (num_r + 1) * sizeof (struct license_result_t));
          r[num_r] = (*results)[i];
          r[num_r].license_family = strdup ((*results)[i].license_family);
          r[num_r].license = strdup ((*results)[i].license);
          r[num_r].cmd = strdup ((*results)[i].cmd);
          num_r++;
        }
    }
  for (int i = 0; i < *num_results; i++)
    {
      free ((*results)[i].license_family);
      free ((*results)[i].license);
      free ((*results)[i].cmd);
    }
  free (*results);
  *results = r;
  *num_results = num_r;

  //we want the mentions to say 100%
  for (int i = 0; i < *num_results; i++)
    {
      if (strstr ((*results)[i].license, "mention"))
        (*results)[i].result = 100;
    }
}

static void display_results (struct lu_state_t *state, struct lu_detect_options_t *options, char *display_filename, struct license_result_t *results, int num_results)
{
      
  char *f = display_filename;
  if (strcmp (f, "-") == 0)
    f = N_("(standard input)");

  if (options->show_most_similar_licenses)
    {
      struct license_result_t *selected = NULL;
      int num_selected = 0;
      for (int i = 0; i < num_results; i++)
        {
          int found = 0;
          for (int j = 0; j < num_selected; j++)
            {
              //check start
              if (selected[j].starting_block >= results[i].starting_block &&
                  selected[j].starting_block <= results[i].ending_block)
                {
                  //okay, the starting block is within this set.
                  //we can have it on the ending block, but that's it.
                  found = 1;
                }
              if (selected[j].ending_block >= results[i].starting_block &&
                  selected[j].ending_block <= results[i].ending_block)
                {
                  //okay, the ending block is within this set.
                  //we can have it on the starting block, but that's it.
                  found = 1;
                }
          
              //mentioned licenses can't overlap at all with licenses.
              //but can overlap with other mentions
              if (found)
                {
                  //if we have overlaps
                  int existing_mention =
                    strstr (selected[j].license, "mention") != NULL;
                  int this_mention =
                    strstr (results[i].license, "mention") != NULL;
                  if (this_mention && existing_mention)
                    found = 0;
                }
            }
          if (!found && results[i].result >= options->limit)
            {
              selected = realloc (selected, (num_selected + 1) *
                                  sizeof (struct license_result_t));
              selected[num_selected] = results[i];
              selected[num_selected].license = strdup (results[i].license);
              selected[num_selected].cmd = strdup (results[i].cmd);
              num_selected++;
            }
        }

      for (int i = 0; i < num_selected; i++)
        {
          if (selected[i].result == 0.0)
            {
              luprintf (state, "%-25s %7.3f%% %s\n",
                        N_("unknown"), selected[i].result, f);
              break;
            }
          if (selected[i].license)
            luprintf (state, "%-25s %7.3f%% %s\n",
                      results[i].license, results[i].result, f);
        }
      if (num_selected == 0)
        luprintf (state, "%-25s %7.3f%% %s\n",
                  N_("unknown"), 0.0, f);
      for (int i = 0; i < num_selected; i++)
        {
          free (selected[i].license);
          free (selected[i].cmd);
        }
      free (selected);
    }
  else
    {
      for (int i = 0; i < num_results; i++)
        {
          if (results[i].result == 0.0)
            {
              luprintf (state, "%-25s %7.3f%% %s\n",
                        N_("unknown"), results[i].result, f);
              break;
            }
          if (results[i].license)
            luprintf (state, "%-25s %7.3f%% %s\n",
                      results[i].license, results[i].result, f);
        }
    }
}

static int
process_boilerplate_blocks (struct lu_state_t *state, struct lu_detect_options_t *options, char *filename, char *display_filename)
{
  int quick = 0;
  lu_block_t *blocks = NULL;
  int num_blocks = 0;
  lu_count_blocks (filename, &blocks, &num_blocks);
  if (blocks[0].ending_line == -1)
    {
      // second chance!
      // we didn't find any text in the uncommented boilerplate
      // so we're just going to check the whole file
      // but it's slow to check the whole file in the normal way,
      // so we only do this if we're doing search for license files instead
      // of notices.
      free (blocks);
      blocks = NULL;
      num_blocks = 0;
      lu_count_blocks (display_filename, &blocks, &num_blocks);
      filename = display_filename;
      quick = 1;
    }
  struct license_result_t *results = NULL;
  int num_results = 0;

  int num_licenses;
  struct lu_license_t *licenses = lu_list_of_licenses (&num_licenses);

  int count = 0;
  char * lic_cache[num_licenses];
  int i = 0;
  while (licenses[i].keyword != NULL)
    {
      char *cmd = licenses[i].cmd;
      char *license_filename = lu_dump_command_to_file (state, cmd);
      FILE *fp = fopen (license_filename, "r");
      if (fp)
        {
          size_t len = 0;
          char *s = fread_file (fp, &len);
          if (s)
            {
              char *squeezed_license = squeeze (s);
              lic_cache[count] = squeezed_license;
              free (s);
            }
          fclose (fp);
        }
      remove (license_filename);
      free (license_filename);
      count++;
      i++;
    }

  int num_mentions;
  struct lu_license_mention_t *mentions =
    lu_list_of_license_mentions (&num_mentions);
  i = 0;
  char * mention_cache[num_mentions];
  count = 0;
  while (mentions[i].words != NULL)
    {
      char *squeezed_license = squeeze (mentions[i].words);
      mention_cache[count] = squeezed_license;
      i++;
      count++;
    }

  //okay here we go

  for (int i = 1 ; i <= num_blocks; i++)
    {
      if (quick && i == 1)
        {
          i = num_blocks - 1;
          if (i == 0)
            i = 1;
        }
      for (int j = 0; j <= num_blocks - (i - 1) - 1; j++)
        {
          char *file = dump_file (filename, blocks, num_blocks, j, j+i);
          if (file)
            {
              size_t len;
              FILE *fp = fopen (file, "r");
              char *s = fread_file (fp, &len);
              fclose (fp);
              remove (file);
              free (file);
              char *s2 = squeeze (s);
              detect_licenses_and_collect_results (state, options, s, s2,
                                                   &results, &num_results,
                                                   j, j+i, lic_cache,
                                                   licenses, num_licenses,
                                                   mentions, num_mentions,
                                                   mention_cache);
              free (s);
              free (s2);

            }
        }
    }


  int found = 0;
  for (int i = 0; i < num_results; i++)
    {
      if (results[i].result >= options->limit)
        {
          found = 1;
          break;
        }
    }
  //second chance
  if (!found && !quick)
    {
      free (blocks);
      blocks = NULL;
      num_blocks = 0;
      lu_count_blocks (display_filename, &blocks, &num_blocks);
      filename = display_filename;
      int i = num_blocks - 1;
      if (i == 0)
        i = 1;
      for (int j = 0; j <= num_blocks - (i - 1) - 1; j++)
        {
          char *file = dump_file (filename, blocks, num_blocks, j, j+i);
          if (file)
            {
              size_t len;
              FILE *fp = fopen (file, "r");
              char *s = fread_file (fp, &len);
              fclose (fp);
              remove (file);
              free (file);
              char *s2 = squeeze (s);
              detect_licenses_and_collect_results (state, options, s, s2,
                                                   &results, &num_results,
                                                   j, j+i, lic_cache,
                                                   licenses, num_licenses,
                                                   mentions, num_mentions,
                                                   mention_cache);
              free (s);
              free (s2);

            }
        }
    }

  for (int i = 0; i < num_licenses; i++)
    free (lic_cache[i]);
  for (int i = 0; i < num_mentions; i++)
    free (mention_cache[i]);
  if (options->cull)
    cull_results (options, &results, &num_results);
  display_results (state, options, display_filename, results, num_results);
      
  for (int i = 0; i < num_results; i++)
    {
      free (results[i].license_family);
      free (results[i].license);
      free (results[i].cmd);
    }
  free (results);
  free (blocks);
  free (licenses);
  free (mentions);
  return 0;
}

static int
detect_stdin (struct lu_state_t *state, struct lu_detect_options_t *options)
{
  int err = 0;
  char tmp[sizeof(PACKAGE) + 13];
  snprintf (tmp, sizeof tmp, "/tmp/%s.XXXXXX", PACKAGE);
  int fd = mkstemp(tmp);
  close (fd);
  FILE *fileptr = fopen (tmp, "w");
  if (fileptr)
    {
      char *line = NULL;
      size_t len = 0;
      ssize_t read;

      while ((read = getline(&line, &len, stdin)) != -1)
        fprintf (fileptr, "%s", line);

      free (line);
      fflush (fileptr);
      fsync (fileno (fileptr));
      fclose (fileptr);
      process_boilerplate_blocks (state, options, tmp, "-");
      remove (tmp);
    }
  else
    err = -1;
  return err;
}

static char *
get_png_boilerplate (struct lu_state_t *state, struct lu_detect_options_t *options, char *filename)
{
  struct lu_png_boilerplate_options_t png_boilerplate_options;
  memset (&png_boilerplate_options, 0, sizeof (png_boilerplate_options));
  argz_add (&png_boilerplate_options.input_files, &png_boilerplate_options.input_files_len, filename);

  char tmp[sizeof(PACKAGE) + 13];
  snprintf (tmp, sizeof tmp, "/tmp/%s.XXXXXX", PACKAGE);
  int fd = mkstemp(tmp);
  close (fd);
  char *tmpext = xasprintf ("%s.%s", tmp, basename (filename));
  rename (tmp, tmpext);
  FILE *fileptr = fopen (tmpext, "w");
  FILE *oldout = state->out;
  state->out = fileptr;
  int err = lu_png_boilerplate (state, &png_boilerplate_options);
  fclose (fileptr);
  state->out = oldout;

  if (err)
    return NULL;
  return tmpext;
}

static char *
get_boilerplate (struct lu_state_t *state, struct lu_detect_options_t *options, char *filename)
{
  struct lu_boilerplate_options_t boilerplate_options;
  memset (&boilerplate_options, 0, sizeof (boilerplate_options));
  boilerplate_options.quiet = 1;
  boilerplate_options.full = options->full;
  argz_add (&boilerplate_options.input_files, &boilerplate_options.input_files_len, filename);

  char tmp[sizeof(PACKAGE) + 13];
  snprintf (tmp, sizeof tmp, "/tmp/%s.XXXXXX", PACKAGE);
  int fd = mkstemp(tmp);
  close (fd);
  char *tmpext = xasprintf ("%s.%s", tmp, basename (filename));
  rename (tmp, tmpext);
  FILE *fileptr = fopen (tmpext, "w");
  FILE *oldout = state->out;
  state->out = fileptr;
  int err = lu_boilerplate (state, &boilerplate_options, NULL);
  state->out = oldout;
  fclose (fileptr);

  struct lu_uncomment_options_t uncomment_options;
  memset (&uncomment_options, 0, sizeof (uncomment_options));
  uncomment_options.style = boilerplate_options.style;
  argz_add (&uncomment_options.input_files, &uncomment_options.input_files_len, tmpext);
  char tmp2[sizeof(PACKAGE) + 13];
  snprintf (tmp2, sizeof tmp, "/tmp/%s.XXXXXX", PACKAGE);
  fd = mkstemp(tmp2);
  close (fd);
  fileptr = fopen (tmp2, "w");
  oldout = state->out;
  state->out = fileptr;
  err = lu_uncomment (state, &uncomment_options);
  state->out = oldout;
  fclose (fileptr);
  remove (tmpext);
  free (tmpext);
  if (err)
    return NULL;
  return strdup (tmp2);
}

static int
detect_uncommented_boilerplate (struct lu_state_t *state, struct lu_detect_options_t *options, char *filename)
{
  char *comment_file = NULL;
  char *ext = strrchr (filename, '.');
  if (ext && strcasecmp (ext, ".png") == 0)
    comment_file = get_png_boilerplate (state, options, filename);
  else
    comment_file = get_boilerplate (state, options, filename);
  int err = process_boilerplate_blocks (state, options, comment_file, filename);
  remove (comment_file);
  free (comment_file);
  return err;
}

int 
lu_detect (struct lu_state_t *state, struct lu_detect_options_t *options)
{
  char *f = NULL;
  int err = 0;
  while ((f = argz_next (options->input_files, options->input_files_len, f)))
    {
      if (strcmp (f, "-") == 0)
        err = detect_stdin (state, options);
      else
        err = detect_uncommented_boilerplate (state, options, f);
      if (err)
        break;
    }
  return err;
}

struct lu_command_t detect = 
{
  .name         = N_("detect"),
  .doc          = DETECT_DOC,
  .flags        = SHOW_IN_HELP | SAVE_IN_HISTORY | IS_MOST_FREQUENTLY_USED,
  .argp         = &argp,
  .parser       = lu_detect_parse_argp
};

/*  Copyright (C) 2013, 2014 Ben Asselstine

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
  02110-1301, USA.
*/
#include <config.h>
#include <stdlib.h>
#include <unistd.h>
#include "licensing_priv.h"
#include "fsf-permissive.h"
#include "gettext-more.h"
#include "xvasprintf.h"
#include "read-file.h"
#include "error.h"
#include "util.h"
#include "url-downloader.h"

static struct argp_option argp_options[] = 
{
    {"brief", 'b', NULL, 0, N_("show the brief version")},
    {"list-license-notices", 'l', NULL, OPTION_HIDDEN, N_("show licenses and exit")},
    {0}
};

static error_t 
parse_opt (int key, char *arg, struct argp_state *state)
{
  struct lu_fsf_permissive_options_t *opt = NULL;
  if (state)
    opt = (struct lu_fsf_permissive_options_t*) state->input;
  switch (key)
    {
    case 'b':
      opt->brief = 1;
      break;
    case 'l':
        {
          int i = 0;
          while (fsf_permissive.notices[i].keyword != NULL)
            {
              fprintf (stdout, "%s\n", fsf_permissive.notices[i].keyword);
              i++;
            }
          exit (0);
        }
      break;
    case ARGP_KEY_INIT:
      opt->brief = 0;
      break;
    default:
      return ARGP_ERR_UNKNOWN;
    }
  return 0;
}

#undef FSF_PERMISSIVE_DOC
#define FSF_PERMISSIVE_DOC N_("Show the FSF Permissive License.")
static struct argp argp = { argp_options, parse_opt, "", FSF_PERMISSIVE_DOC};

int 
lu_fsf_permissive_parse_argp (struct lu_state_t *state, int argc, char **argv)
{
  int err = 0;
  struct lu_fsf_permissive_options_t opts;
  opts.state = state;

  err = argp_parse (&argp, argc, argv, state->argp_flags,  0, &opts);
  if (!err)
    return lu_fsf_permissive (state, &opts);
  else
    return err;
}


int
show_lu_fsf_permissive(struct lu_state_t *state, struct lu_fsf_permissive_options_t *options)
{
  // i can't find an authoritative place for this, so here it is.
  if (options->brief)
      luprintf (state, "\
This file is free software; the Free Software Foundation\n\
gives unlimited permission to copy, distribute and modify it.\n");
  else
      luprintf (state, "\
This file is free software; the Free Software Foundation\n\
gives unlimited permission to copy and/or distribute it,\n\
with or without modifications, as long as this notice is preserved.\n\
\n\
This program is distributed in the hope that it will be useful,\n\
but WITHOUT ANY WARRANTY, to the extent permitted by law; without\n\
even the implied warranty of MERCHANTABILITY or FITNESS FOR A\n\
PARTICULAR PURPOSE.\n");
  return 0;
}

int 
lu_fsf_permissive (struct lu_state_t *state, struct lu_fsf_permissive_options_t *options)
{
  int err = 0;
  err = show_lu_fsf_permissive(state, options);
  return err;
}

struct lu_command_t fsf_permissive = 
{
  .name         = N_("fsf-permissive"),
  .doc          = FSF_PERMISSIVE_DOC,
  .flags        = IS_A_LICENSE | SAVE_IN_HISTORY | SHOW_IN_HELP,
  .argp         = &argp,
  .parser       = lu_fsf_permissive_parse_argp,
  .latest_idx   = 0,
  .notices      =
    {
        {
          .keyword   = "fsf-permissive",
          .cmd       = "fsf-permissive",
          .words     = "unlimited,Foundation,preserved",
          .recommend = 1
        },
        {
          .keyword   = "fsf-permissive-brief",
          .cmd       = "fsf-permissive --brief",
          .words     = "unlimited,Foundation",
          .recommend = 1
        },
        {
          .keyword   = NULL,
          .cmd       = NULL,
          .words     = NULL,
          .recommend = 0
        },
    }
};
